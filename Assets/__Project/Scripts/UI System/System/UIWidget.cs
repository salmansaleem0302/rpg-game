using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(GraphicRaycaster))]
[RequireComponent(typeof(CanvasScaler))]
[RequireComponent(typeof(CanvasGroup))]

public abstract class UIWidget : MonoBehaviour
{
    public virtual void Init()
    {
        
    }

    public virtual void Uninitialise()
    {
        
    }
}
