﻿using UnityEngine;

namespace FarrokhGames.Inventory
{
    [RequireComponent(typeof(Inven_Renderer))]
    public class InventoryExample : MonoBehaviour
    {
        [SerializeField] private Inven_RenderMode _renderMode = Inven_RenderMode.Grid;
        [SerializeField] private int _maximumAlowedItemCount = -1;
        [SerializeField] private ItemType _allowedItem = ItemType.Any;
        [SerializeField] private int _width = 8;
        [SerializeField] private int _height = 4;
        [SerializeField] private ItemDefinition[] _definitions = null;
        [SerializeField] private bool _fillRandomly = true; 
        [SerializeField] private bool _fillEmpty = false;
        InventoryRequirements provider;
        public Inven_Manager inven_Manager;
        public Transform weaponHand;

        protected virtual void Start()
        {
            provider = new InventoryRequirements(_renderMode, _maximumAlowedItemCount, _allowedItem);
            inven_Manager = new Inven_Manager(provider, _width, _height);

            RenderInventory();

            FillRandomly();
            FillEmpty();

            OnWeaponSlot();
        }

        void OnWeaponSlot()
        {
            if (_allowedItem == ItemType.Weapons)
                inven_Manager.onItemAdded += EquipWeapon;
        }

        void EquipWeapon(IInven_Item item)
        {
            ItemDefinition addedItem = item as ItemDefinition;

            /*GameObject equipModuel = Instantiate(addedItem.dropObject);
            equipModuel.GetComponent<Collider>().enabled = false;
            equipModuel.GetComponent<Rigidbody>().isKinematic = true;
            equipModuel.transform.SetParent(weaponHand);
            equipModuel.transform.localPosition = new Vector3(0, 0, 0);*/
        }

        public void RenderInventory()
        {
            GetComponent<Inven_Renderer>().SetInventory(inven_Manager, provider.inventoryRenderMode);
            transform.GetChild(0).gameObject.SetActive(true);
        }

        void FillRandomly()
        {
            if (_fillRandomly)
            {
                var tries = (_width * _height) / 3;
                for (var i = 0; i < tries; i++)
                {
                    inven_Manager.TryAdd(_definitions[Random.Range(0, _definitions.Length)].CreateInstance());
                }
            }
        }

        void FillEmpty()
        {
            if (_fillEmpty)
            {
                for (var i = 0; i < _width * _height; i++)
                {
                    inven_Manager.TryAdd(_definitions[0].CreateInstance());
                }
            }
        }

        public void TryAdd(IInven_Item itemDefinition)
        {
            inven_Manager.TryAdd(itemDefinition);
        }
    }
}