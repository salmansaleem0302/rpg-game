using UnityEngine;
using UnityEngine.UI;
using Player_Actions;

namespace FarrokhGames.Inventory.Examples
{
    public class Inven_Initialation : MonoBehaviour
    {
        bool isFirst_Time;
        HorizontalLayoutGroup horizontalLayout;
        public GameObject playerInven;

        private void Start()
        {
            isFirst_Time = false;
            horizontalLayout = GetComponent<HorizontalLayoutGroup>();
        }

        public void Render_Inventory()
        {
            if (isFirst_Time == false)
            {
                InventoryExample[] InventoryExamples = gameObject.GetComponentsInChildren<InventoryExample>();
                foreach (InventoryExample inventoryExample in InventoryExamples)
                {
                    inventoryExample.RenderInventory();
                }

                horizontalLayout.enabled = true;
                isFirst_Time = true;
            }
        }

        public GameObject PlayerInventory()
        {
            return playerInven;
        }
    }
}
