using Player_Actions;
using UnityEngine;

namespace FarrokhGames.Inventory
{
    public class Player_Inven_Controller : Inven_Controller
    {
        PlayerActions playerActions;
        bool isTrade = false;

        void Start()
        {
            playerActions = FindObjectOfType(typeof(PlayerActions)) as PlayerActions;
        }

        public void Add_OnSell_RecieveCoins()
        {
            isTrade = true;
            onItemAdded_To_Other_Inven += RecieveCoins;
        }

        public void Disable_Trade()
        {
            isTrade = false;
            onItemAdded_To_Other_Inven -= RecieveCoins;
        }

        void RecieveCoins(IInven_Item item)
        {
            if (TradeManager.originalController == this)
                playerActions.totalCoins = playerActions.totalCoins + item.price;
        }

        protected override void Item_Dropped()
        {
            if (isTrade)
            {
                inventory.TryAddAt(_itemToDrag, _draggedItem.originPoint);
                onItemReturned?.Invoke(_itemToDrag);
            }
            else
            {
                base.Item_Dropped();
            }
        }
    }
}