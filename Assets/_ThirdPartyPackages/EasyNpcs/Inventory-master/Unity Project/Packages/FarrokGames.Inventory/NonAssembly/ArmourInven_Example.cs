using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FarrokhGames.Inventory
{
    public class ArmourInven_Example : InventoryExample
    {
        public SkinnedMeshRenderer skinnedMeshRenderer;
        public Mesh armour;

        protected override void Start()
        {
            base.Start();
            inven_Manager.onItemAdded += EquipArmour;
        }

        void EquipArmour(IInven_Item item)
        {
            skinnedMeshRenderer.sharedMesh = armour;
        }
    }
}