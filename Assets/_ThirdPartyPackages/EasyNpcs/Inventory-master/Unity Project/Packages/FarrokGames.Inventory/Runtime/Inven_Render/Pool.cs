using System.Collections.Generic;
using System;
using System.Linq;

namespace FarrokhGames.Shared
{
    public sealed class Pool<T> where T : class
    {
        private List<T> inactive_imageObjects = new List<T>();
        private List<T> active_imageObjects = new List<T>();
        private Func<T> default_image_object;

        public Pool(Func<T> imageObject, int initialCount = 0, bool allowTakingWhenEmpty = true)
        {
            if (imageObject == null) throw new ArgumentNullException("pCreator");
            if (initialCount < 0) throw new ArgumentOutOfRangeException("pInitialCount", "Initial count cannot be negative");

            default_image_object = imageObject;
            inactive_imageObjects.Capacity = initialCount;
            CanTake = allowTakingWhenEmpty;

            Create_DefaultImageObject_Much_As_InitialCount(initialCount);
        }

        void Create_DefaultImageObject_Much_As_InitialCount(int initialCount)
        {
            while (inactive_imageObjects.Count < initialCount)
            {
                inactive_imageObjects.Add(default_image_object());
            }
        }

        public int Count => inactive_imageObjects.Count;
        public bool CanTake;

        public T Create_ImageObject_In_Pool()
        {
            var obj = default_image_object();
            active_imageObjects.Add(obj);
            return obj;
        }

        public List<T> GetInactive()
        {
            return inactive_imageObjects.ToList();
        }

        public List<T> GetActive()
        {
            return active_imageObjects.ToList();
        }
    }
}