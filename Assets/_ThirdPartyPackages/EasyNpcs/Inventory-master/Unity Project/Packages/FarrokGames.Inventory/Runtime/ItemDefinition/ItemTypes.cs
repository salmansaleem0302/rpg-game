namespace FarrokhGames.Inventory
{
    public enum ItemType
    {
        Any,
        Weapons,
        Armour,
        Utility,
    }
}