using UnityEngine;
using FarrokhGames.Inventory;
using FarrokhGames.Inventory.Examples;

public class Item : MonoBehaviour
{
    public ItemDefinition ItemDefinition;

    public IInven_Item CreateInstance()
    {
        return ItemDefinition.CreateInstance();
    }
}
