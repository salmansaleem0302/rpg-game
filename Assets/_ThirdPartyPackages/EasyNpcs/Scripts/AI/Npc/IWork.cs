using UnityEngine;

public interface IWork 
{
    public Behaviour GetScript();
}
