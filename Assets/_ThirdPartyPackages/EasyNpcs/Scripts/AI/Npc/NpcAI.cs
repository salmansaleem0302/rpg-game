﻿using System;
using UnityEngine;

namespace Npc_AI
{
    public class NpcAI : NpcData, IAttackable
    {
        public float movementSpeed;
        public float scaredRunningSpeed;
        public float runningDistance;
        public float runningTime;

        [HideInInspector]
        public TextMesh Text;

        DayAndNightControl dayAndNightControl;
        Behaviour workScript;
        public Transform home;
        public Transform work;

        private NpcState _currentState;

        public NpcState currentState
        {
            get
            {
                return _currentState;
            }
            protected set
            {
                _currentState = value;
            }
        }

        protected override void Start()
        {
            base.Start();
            Text = GetComponentInChildren<TextMesh>();
            DayAndNightCycle_Initialize();
            workScript = GetComponent<Work>();
        }

        void DayAndNightCycle_Initialize()
        {
            dayAndNightControl = FindObjectOfType<DayAndNightControl>();

            if (dayAndNightControl != null)
            {
                dayAndNightControl.OnMorningHandler += GoToWork;
                dayAndNightControl.OnEveningHandler += GoHome;
            }
            else
            {
                Debug.Log("Add in dayAndNight control to scene for use of npc's life cycle");
            }
        }

        void Update()
        {
            WatchEnvironment();
        }

        GameObject attacker;

        void WatchEnvironment()
        {
            attacker = NpcSense.NPC_Sense_Attacked(transform.position, VisionRange, VisionLayers);
            if (attacker != null)
            {
                ChangeState(NpcState.Scared);
            }
            else
            {
                TriggerConversation(NpcSense.Sense_Nearby_Npc(transform.position, 5, VisionLayers, this));
            }
        }

        public void ChangeState(NpcState newState)
        {
            if (currentState == newState)
                return;

            NpcState prevState = currentState;
            currentState = newState;

            OnStateChanged(prevState, newState);
        }

        private void OnStateChanged(NpcState prevState, NpcState newState)
        {
            TurnOffBehaviour(prevState);
            switch (newState)
            {
                case NpcState.Scared:
                    OnScared();
                    break;

                case NpcState.GoingHome:
                    GoHome();
                    break;

                case NpcState.GoingToWork:
                    break;

                case NpcState.Idle:
                    OnIdle();
                    break;

                case NpcState.Talking:
                    agent.SetDestination(transform.position);
                    break;

                case NpcState.Working:
                    if (workScript == null)
                        agent.SetDestination(work.position);
                    else
                        workScript.enabled = true;
                    break;

                default: break;
            }
        }

        void TurnOffBehaviour(NpcState prevState)
        {
            switch (prevState)
            {
                case NpcState.Scared:
                    break;
                case NpcState.GoingToWork:
                    Destroy(GetComponent<LifeCycle>());
                    break;
                case NpcState.GoingHome:
                    Destroy(GetComponent<LifeCycle>());
                    break;
                case NpcState.Working:
                    if (workScript != null)
                        workScript.enabled = false;
                    break;
                case NpcState.Talking:
                    EndConversation();
                    break;
                default:
                    break;
            }
        }

        public void OnAttack(GameObject attacker, Attack attack)
        {
            this.attacker = attacker;
            ChangeState(NpcState.Scared);
        }

        void OnScared()
        {
            if (GetComponent<Choose_RunAway_Point>() == null)
            {
                gameObject.AddComponent(typeof(Choose_RunAway_Point));
                if (!GetComponent<CharacterStats>().isDead)
                    StartCoroutine(GetComponent<Choose_RunAway_Point>().Run(attacker));
                else
                    GetComponent<Choose_RunAway_Point>().attacker = attacker;
            }
        }

        void OnIdle()
        {
            if (dayAndNightControl != null)
            {
                float time = dayAndNightControl.currentTime;
                if (time > .3f && time < .7f)
                {
                    GoToWork();
                }
                else
                {
                    GoHome();
                }
            }
        }

        void GoToWork()
        {
            if (this.enabled && Is_Changeable_State())
            {
                ChangeState(NpcState.GoingToWork);
                Set_Cycle_Class().Start_GOTOWork();
            }
        }

        void GoHome()
        {
            if (this.enabled && Is_Changeable_State())
            {
                ChangeState(NpcState.GoingHome);
                Set_Cycle_Class().Start_GOTOHome();
            }
        }

        bool Is_Changeable_State()
        {
            if (currentState == NpcState.GoingHome || currentState == NpcState.Talking || currentState == NpcState.Scared)
                return false;
            else
                return true;
        }

        LifeCycle Set_Cycle_Class()
        {
            LifeCycle lifeCycle = gameObject.AddComponent<LifeCycle>();
            lifeCycle.Set(this);

            return lifeCycle;
        }

        public Job job;
        public Gender gender;

        [Range(0, 5)]
        public int converChoose = 0;

        void TriggerConversation(NpcAI npc)
        {
            if (CheckConditions(npc))
            {
                RunConversation runConversation = gameObject.AddComponent<RunConversation>();
                runConversation.Set(npc, true);
                runConversation.StartConversation();

                ChangeState(NpcState.Talking);
            }
        }

        bool CheckConditions(NpcAI npc)
        {
            if (npc == null)
                return false;
            if (currentState == NpcState.Scared || npc.currentState == NpcState.Scared)
                return false;
            if (UnityEngine.Random.Range(0, 10000) > converChoose)
                return false;
            if (GetInstanceID() < npc.GetInstanceID())
                return false;
            if (GetComponent<RunConversation>() != null || npc.GetComponent<RunConversation>() != null)
                return false;

            return true;
        }

        public void EndConversation()
        {
            Destroy(GetComponent<RunConversation>());
            GetComponentInChildren<TextMesh>().text = null;
        }

        private void OnEnable()
        {
            ChangeState(NpcState.Idle);
        }

        void OnDisable()
        {
            TurnOffBehaviour(currentState);
            anim.SetFloat("Speed", 0);
        }
    }
}