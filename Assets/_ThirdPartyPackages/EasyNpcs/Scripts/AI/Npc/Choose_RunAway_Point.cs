using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Npc_AI;

public class Choose_RunAway_Point : MonoBehaviour
{
    NavMeshAgent agent;
    NpcAI npc;

    public GameObject attacker;

    private float runTimeLeft = 10;

    private void Update()
    {
        runTimeLeft -= Time.deltaTime;
    }

    public IEnumerator Run(GameObject attacker)
    {
        this.attacker = attacker;
        Initalize();

        while (runTimeLeft > 0)
        {
            StartCoroutine(CalculatePath());

            yield return new WaitUntil(() => Vector3.Distance(agent.destination, transform.position) <= npc.runningDistance / 1.2);
        }
        
        npc.ChangeState(NpcState.Idle);
        Destroy(this);
    }

    void Initalize()
    {
        agent = GetComponent<NavMeshAgent>();
        npc = GetComponent<NpcAI>();

        agent.speed = npc.scaredRunningSpeed;
        runTimeLeft = npc.runningTime;
        agent.ResetPath();
    }

    IEnumerator CalculatePath()
    {
        Vector3 goal;
        bool isPathValid;
        NavMeshPath path = new NavMeshPath();

        double[] angleXY = CalculateDirectionOfPoint();
        double angleX = angleXY[0];
        double angleY = angleXY[1];

        int index = 0;
        const int limit = 13;

        do
        {
            angleX += index * Math.Pow(-1.0f, index) * Math.PI / 6.0f;
            angleY -= index * Math.Pow(-1.0f, index) * Math.PI / 6.0f;
            Vector2 direction = new Vector2((float)Math.Cos(angleX), (float)Math.Sin(angleY));
            goal = new Vector3(transform.position.x - direction.x * npc.runningDistance, transform.position.y, transform.position.z - direction.y * npc.runningDistance);

            bool samplePosition = NavMesh.SamplePosition(goal, out NavMeshHit hit, npc.runningDistance / 5, agent.areaMask);
            if (samplePosition)
            {
                agent.CalculatePath(hit.position, path);
                yield return new WaitUntil(() => path.status != NavMeshPathStatus.PathInvalid);
                agent.path = path;
            }

            isPathValid = (samplePosition &&
                           path.status != NavMeshPathStatus.PathPartial &&
                           agent.remainingDistance <= npc.runningDistance);

            if (++index > limit)
            {
                agent.destination = this.transform.position;
                break;
            }
        } while (!isPathValid);
    }

    double[] CalculateDirectionOfPoint()
    {
        Vector3 attackerToNpc = attacker.transform.position - transform.position;
        float twoDimension_magnitude = new Vector2(attackerToNpc.x, attackerToNpc.z).magnitude;
        double angleX = Math.Acos(attackerToNpc.x / twoDimension_magnitude);
        double angleY = Math.Asin(attackerToNpc.z / twoDimension_magnitude);
        double[] anglesXY = {angleX, angleY};

        return anglesXY;
    }
}
