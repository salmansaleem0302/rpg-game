using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using Rotation;

namespace Npc_AI
{
    public class EnemyAI : NpcData, IAttackable
    {
        public Collider patrolArea;
        public Transform attackPoint; 

        [TagSelector] public List<string> Tags;
        public List<string> Protects;

        public EnemeyState currentState;
        public Transform currentTarget;

        public float AttackDistance;

        public enum Weapon { melee, ranged};
        public Weapon assignedWeapon; 
        public Projectile projectile;
        public float launchHight;

        #region Editor Only

#if UNITY_EDITOR
        Transform DebugSphere;
#endif
        #endregion

        protected override void Start()
        {
            base.Start();
            ChangeState(EnemeyState.Idle);

            if (VisionRange <= 0)
            {
                Debug.LogWarning("VisionRange is 0");
            }
        }

        void Update()
        {
            State_On_Update();
        }

        void State_On_Update()
        {
            switch (currentState)
            {
                case EnemeyState.Patrol:
                    OnPatrol();
                    break;

                case EnemeyState.Idle:
                    OnIdle();
                    break;

                case EnemeyState.Chase:
                    OnChase();
                    break;

                case EnemeyState.Attack:
                    OnAttack();
                    break;

                default:
                    break;
            }
        }

        void OnPatrol()
        {
            if (currentTarget == null)
            {
                TryToFindTarget();
            }
            else
            {
                ChangeState(EnemeyState.Chase);
            }
        }

        void TryToFindTarget()
        {
            Transform target = CombatAISense.CheckForTargets(this); 
            if (target != null)
            {
                currentTarget = target;
                ChangeState(EnemeyState.Chase);

                return;
            }
            
            if (Protect() == null)
                No_Target_Available();
        }

        private Transform Protect()
        {
            currentTarget = CombatAISense.BattleAI_Sense_Friendly_Attacked(transform.position, VisionRange, VisionLayers, Protects);
            return currentTarget;
        }

        void No_Target_Available()
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                ChangeState(EnemeyState.Idle);
            }
        }

        public float walkSpeed = 2;

        void OnIdle()
        {
            agent.speed = walkSpeed;
            if (attackPoint == null)
            {
                PatrolToAnotherSpot();
            }
            else
            {
                ChangeState(EnemeyState.Patrol);
            }
        }

        public float runSpeed = 4;

        void OnChase()
        {
            agent.speed = runSpeed;
            
            if (currentTarget.GetComponent<CharacterStats>().isDead == false)
            {
                if (CombatAISense.Check_Target_Distance_And_Raycast(transform, currentTarget, AttackDistance))
                {
                    ChangeState(EnemeyState.Attack);
                }
                else
                {
                    Chase(currentTarget);
                }
            }
        }

        void Chase(Transform target)
        {
            if (agent.destination != currentTarget.position)
            {
                currentTarget = target;
                agent.SetDestination(target.position);
            }
        }

        void OnAttack()
        {
            agent.SetDestination(transform.position);
            OnTargetDead();

            if (CombatAISense.Check_Target_Distance_And_Raycast(transform, currentTarget, AttackDistance))
            {
                Trigger_Attack_Anim(currentTarget.gameObject);
            }
            else
            {
                ChangeState(EnemeyState.Chase);
            }
        }

        void OnTargetDead()
        {
            if (currentTarget.GetComponent<CharacterStats>().isDead == true)
            {
                currentTarget = null;
                ChangeState(EnemeyState.Idle);

                return;
            }
        }

        void Trigger_Attack_Anim(GameObject target)
        {
            anim.SetTrigger("Attack");
        }

        void ChangeState(EnemeyState newState)
        {
            if (currentState == newState)
                return;

            TurnOffBehaviour(currentState);
            OnStateChanged(currentState, newState);
            currentState = newState;
        }

        void OnStateChanged(EnemeyState oldState, EnemeyState newState)
        {
            switch (newState)
            {
                case EnemeyState.Attack:
                    Rotate rotate = gameObject.AddComponent<Rotate>();
                    rotate.RotateTo(currentTarget);
                    break;
                case EnemeyState.Chase:
                    break;
                case EnemeyState.Idle:
                    break;
                case EnemeyState.Patrol:
                    To_Attack_Point();
                    break;
            }
        }

        void TurnOffBehaviour(EnemeyState prevState)
        {
            switch (prevState)
            {
                case EnemeyState.Attack:
                    Destroy(GetComponent<Rotate>());
                    break;
            }
        }

        void To_Attack_Point()
        {
            if (attackPoint)
            {
                agent.SetDestination(attackPoint.position + new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10)));
            }
        }

        void PatrolToAnotherSpot()
        {
            Vector3 dest;
            if (CalculatePatrol.CalculateSpots(this, 25, out dest))
            {
                ChangeState(EnemeyState.Patrol);
                agent.SetDestination(dest);
            }
            else
            {
                ChangeState(EnemeyState.Idle);
            }
        }

        public void OnAttack(GameObject attacker, Attack attack)
        {
            currentTarget = attacker.transform;
        }
    }
}
