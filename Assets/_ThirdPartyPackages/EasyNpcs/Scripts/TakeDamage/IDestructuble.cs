﻿using UnityEngine;

public interface IAttackable
{
    void OnAttack(GameObject attacker, Attack attack);

    void OnDestruction(GameObject destroyer);
}
