﻿using UnityEngine;
using UnityEngine.UI;
using Npc_AI;

public class TextAndButtons : MonoBehaviour
{
    [HideInInspector]
    public GameObject text;

    [HideInInspector]
    public GameObject[] buttons;

    DialogueManager dialogueManager;

    private void Awake()
    {
        text = transform.GetChild(0).gameObject;
        
        buttons = new GameObject[4];
        buttons[0] = transform.GetChild(1).gameObject;
        buttons[1] = transform.GetChild(2).gameObject;
        buttons[2] = transform.GetChild(3).gameObject;
        buttons[3] = transform.GetChild(4).gameObject;
    }

    public void Start_Dialogue(DialogueManager given_Manager)
    {
        dialogueManager = given_Manager;
        ChangeText(dialogueManager.currentSentence.npcText);

        text.SetActive(true);
        dialogueManager.enabled = true;
        dialogueManager.GetComponent<NpcData>().enabled = false;
    }

    public void End_Dialgue()
    {
        text.SetActive(false);
        dialogueManager.enabled = false;
        dialogueManager.GetComponent<NpcData>().enabled = true;
    }

    public void ChangeText(string npcText)
    {
        text.GetComponent<Text>().text = npcText;
    }

    public void Change_To_NextSentence()
    {
        dialogueManager.currentSentence = dialogueManager.currentSentence.nextSentence;
        ChangeText(dialogueManager.currentSentence.npcText);
    }

    public void Disable_Text_For_Choices()
    {
        text.SetActive(false);
        Activate_Choices();
    }

    void Activate_Choices()
    {
        int choiceNum = 0;
        foreach (GameObject button in buttons)
        {
            if (dialogueManager.currentSentence.choices.Count > choiceNum)
            {
                button.SetActive(true);
                button.GetComponentInChildren<Text>().text = dialogueManager.currentSentence.choices[choiceNum].playerText;
            }
            else
            {
                break;
            }

            choiceNum++;
        }
    }

    public void PressButton(int i)
    {
        Disable_Buttons();

        dialogueManager.currentSentence = dialogueManager.currentSentence.choices[i];
        ChangeText(dialogueManager.currentSentence.npcText);
    }

    void Disable_Buttons()
    {
        foreach (GameObject button in buttons)
        {
            button.SetActive(false);
        }

        text.SetActive(true);
    }
}
