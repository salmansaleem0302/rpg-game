using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player_Actions;

public class ShopKeeper_DialogueManager : DialogueManager
{
    public Sentence defaultSentence;
    public Sentence enableTrade;

    protected override void OnDisable()
    {
        base.OnDisable();

        if (currentSentence == enableTrade)
        {
            playerScripts.GetComponent<PlayerActions>().EnableTrade(gameObject);
            currentSentence = defaultSentence;
        }
    }
}
