using UnityEngine;
using UnityEngine.AI;
using Rotation;

public class DialogueManager : MonoBehaviour
{
    public Sentence currentSentence;
    protected Transform player;
    protected Transform playerScripts;

    void Awake()
    {
        player = GameObject.FindWithTag("Player").transform;
        playerScripts = player.transform.GetChild(1);
    }

    private void OnEnable()
    {
        GetComponent<NavMeshAgent>().SetDestination(transform.position);
        Rotate rotate = gameObject.AddComponent<Rotate>();
        rotate.RotateTo(player);
    }

    protected virtual void OnDisable()
    {
        Destroy(GetComponent<Rotate>());
    }
}
