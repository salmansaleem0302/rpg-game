﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Npc_AI;

namespace Stats_Player
{
    public class PlayerStats : CharacterStats
    {
        public Slider slider;

        protected override void Start()
        {
            base.Start();
            slider.maxValue = maxHealth.GetValue();
        }

        public override void OnAttack(GameObject attacker, Attack attack)
        {
            base.OnAttack(attacker, attack);
            slider.value = currentHealth.GetValue();
        }

        protected override void OnDeath(GameObject attacker)
        {
            MonoBehaviour[] monoBehaviours = GetComponentsInChildren<MonoBehaviour>();
            foreach (MonoBehaviour monoBehaviour in monoBehaviours)
            {
                monoBehaviour.enabled = false;
            }
        }
    }
}