using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player_Actions;

public class OnAttackAnimPlayer : StateMachineBehaviour
{
    PlayerActions playerActions;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerActions = animator.GetComponentInParent<PlayerActions>();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (playerActions != null)
        {
            On_Player(animator);
        }
    }

    void On_Player(Animator animator)
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, Mathf.Infinity, ~LayerMask.GetMask("Player")))
        {
            AttackManager.AttackTarget(animator.transform.root.gameObject, hit.collider.gameObject);
            Instantiate(animator.GetBehaviour<OnAttackAnimAI>().swordSound);
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
