using UnityEngine;

public class PlayerAnim : MonoBehaviour
{
    Animator anim;
    public CharacterController characterController;

    AudioSource audioSource;
    public AudioClip clip;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();    
    }

    void Update()
    {
        anim.SetFloat("Speed", characterController.velocity.magnitude);

        if (characterController.velocity.magnitude > 0)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.clip = clip;
                audioSource.Play();
            }
        }
        else
        {
            audioSource.clip = null;
            audioSource.Stop();
        }
    }
}