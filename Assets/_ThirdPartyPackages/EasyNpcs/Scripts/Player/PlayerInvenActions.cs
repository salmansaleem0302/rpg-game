using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FarrokhGames.Inventory;
using FarrokhGames.Inventory.Examples;

public class PlayerInvenActions : MonoBehaviour
{
    public GameObject inventoryParent;

    Inven_Initialation initialation;
    bool not_Rendered;

    public InventoryExample playerInvenExample;
    public Player_Inven_Controller playerInvenController;
    public InventoryExample tradeInvenExample;

    public GameObject weaponUI;
    public GameObject armourUI;
    GameObject tradeInventory;

    private void Start()
    {
        initialation = inventoryParent.GetComponent<Inven_Initialation>();
        not_Rendered = true;

        tradeInventory = tradeInvenExample.gameObject;
        
        StartCoroutine(Close_Inven_After_One_Frame());
    }

    IEnumerator Close_Inven_After_One_Frame()
    {
        yield return new WaitForEndOfFrame();
        
        inventoryParent.SetActive(false);
    }

    public void Grab_Item(GameObject item)
    {
        playerInvenExample.TryAdd(item.GetComponent<Item>().CreateInstance());
        Destroy(item);
    }

    public void Activate_Trade(Goods_For_Trade storedGoods)
    {
        tradeInventory.SetActive(true);
        
        foreach (ItemDefinition item in storedGoods.itemDefinitions)    
        {
            tradeInvenExample.TryAdd(item);
        }

        playerInvenController.Add_OnSell_RecieveCoins();
    }

    public void Activate_Inventory(bool on)
    {
        if (on == true)
        {
            First_Inventory_Load_Initialize();
        }
        else
        {
            playerInvenController.Disable_Trade();
        }

        inventoryParent.SetActive(on);
        tradeInventory.SetActive(false);
    }

    public void Activacte_Weapon_UI(bool on)
    {
        weaponUI.SetActive(on);
        armourUI.SetActive(on);
    }

    private void First_Inventory_Load_Initialize()
    {
        if (not_Rendered)
        {
            initialation.Render_Inventory();
            not_Rendered = false;
        }
    }
}