using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Player_Actions;

public class Coins : MonoBehaviour
{
    PlayerActions playerActions;

    private void Start()
    {
        playerActions = FindObjectOfType<PlayerActions>();
    }

    void Update()
    {
        GetComponent<Text>().text = playerActions.totalCoins.ToString();
    }
}
