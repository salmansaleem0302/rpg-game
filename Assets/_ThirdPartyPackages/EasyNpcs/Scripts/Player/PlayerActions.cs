using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Npc_AI;
using FarrokhGames.Inventory.Examples;

namespace Player_Actions
{
    public class PlayerActions : MonoBehaviour
    {
        public Camera playerCamera;

        public GameObject dialogueWindow;
        TextAndButtons textAndButtons;

        public KeyCode InteractButton = KeyCode.E;
        public KeyCode InventoryButton = KeyCode.Tab;
        public KeyCode QuestButton = KeyCode.Q;

        enum PlayerState { Default, Dialogue, Inventory, Trade, Quest }
        PlayerState playerState;

        PlayerInvenActions playerInvenActions;

        public int totalCoins;

        private void Start()
        {
            playerState = PlayerState.Default;
            
            dialogueWindow = FindObjectOfType<TextAndButtons>().gameObject;
            textAndButtons = dialogueWindow.GetComponent<TextAndButtons>();
            playerInvenActions = GetComponent<PlayerInvenActions>();
        }

        void Update()
        {
            switch (playerState)
            {
                case PlayerState.Default:
                    Attack();
                    Interact();
                    On_InventoryButton_Down(true);
                    EnableQuestUI(true);
                    break;
                case PlayerState.Dialogue:
                    On_Dialgue_Sequence();
                    break;
                case PlayerState.Inventory:
                    On_InventoryButton_Down(false);
                    break;
                case PlayerState.Trade: 
                    On_InventoryButton_Down(false);
                    break;
                case PlayerState.Quest:
                    EnableQuestUI(false);
                    break;
            }
        }

        public GameObject questUI;

        void EnableQuestUI(bool on)
        {
            if (Input.GetKeyDown(QuestButton))
            {
                if (on)
                {
                    playerState = PlayerState.Quest;
                    Turn_Off_Player_Script(true);
                    questUI.SetActive(true);
                }
                else
                {
                    playerState = PlayerState.Default;
                    Turn_Off_Player_Script(false);
                    questUI.SetActive(false);
                }
            }
        }

        void Attack()
        {
            if (Input.GetMouseButtonUp(0))
            {
                Animator animator = GetComponentInChildren<Animator>();
                animator.SetTrigger("Attack");
            }
        }

        public float interactionRange = 2;

        void Interact()
        {
            if (Input.GetKeyDown(InteractButton))
            {
                if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out RaycastHit hit, interactionRange))
                {
                    GameObject chosenObject = hit.transform.gameObject;
                    if (CheckState.Check_CharacterManager(chosenObject))
                    {
                        NpcInteract(chosenObject);
                    }
                    else
                    {
                        playerInvenActions.Grab_Item(chosenObject);
                    }
                }
            }
        }

        void NpcInteract(GameObject npc)
        {
            if (npc.GetComponent<DialogueManager>().currentSentence != null)
            {
                StartDialogue(npc);
            }
            else
            {
                EnableTrade(npc);
            }
        }

        public void EnableTrade(GameObject npc)
        {
            playerState = PlayerState.Trade;

            Turn_Off_Player_Script(true);
            playerInvenActions.Activate_Inventory(true);
            playerInvenActions.Activate_Trade(npc.GetComponent<Goods_For_Trade>());
            playerInvenActions.Activacte_Weapon_UI(false);
        }

        DialogueManager npc_Dialogue;

        void StartDialogue(GameObject npc)
        {
            if (npc.GetComponent<DialogueManager>() != null)
            {
                npc_Dialogue = npc.GetComponent<DialogueManager>();
                if (CheckState.Check_State(npc))
                {
                    Turn_DialogueState(true);
                    textAndButtons.Start_Dialogue(npc.GetComponent<DialogueManager>()); 
                }
            }
        }

        void On_Dialgue_Sequence()
        {
            if (Input.GetMouseButtonUp(0))
            {
                Change_State_Of_Dialogue();
            }
        }

        void Change_State_Of_Dialogue()
        {
            if (npc_Dialogue.currentSentence.nextSentence != null)
            {
                textAndButtons.Change_To_NextSentence();
            }
            else if (npc_Dialogue.currentSentence.choices != null)
            {
                textAndButtons.Disable_Text_For_Choices();
            }
            else
            {
                Turn_DialogueState(false);
            }
        }

        void Turn_DialogueState(bool on)
        {
            Change_State_To_Dialogue(on);
            Turn_Off_Player_Script(on);

            if (on == false)
            {
                textAndButtons.End_Dialgue();
            }
        }

        void Change_State_To_Dialogue(bool on)
        {
            if (on)
                playerState = PlayerState.Dialogue;
            else
                playerState = PlayerState.Default;
        }

        void On_InventoryButton_Down(bool on)
        {
            if (Input.GetKeyDown(InventoryButton))
            {
                Turn_Off_Player_Script(on);
                playerInvenActions.Activate_Inventory(on);
                playerInvenActions.Activacte_Weapon_UI(on);

                if (on)
                {
                    playerState = PlayerState.Inventory;
                }
                else
                {
                    playerState = PlayerState.Default;
                }
            }
        }

        public PlayerMovement playerMovement;
        public MouseLook mouseLook;

        void Turn_Off_Player_Script(bool on)
        {
            playerMovement.enabled = !on;
            mouseLook.enabled = !on;
            CursorManager.SetCursor(on);
        }
    }
}
